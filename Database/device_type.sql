/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/01/2022 16:03:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for device_type
-- ----------------------------
DROP TABLE IF EXISTS `device_type`;
CREATE TABLE `device_type`  (
  `Id` int(0) NOT NULL COMMENT '主键',
  `DeviceTypeName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '类型名称',
  `Sign` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标记字母',
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `uk_devicetype_sign`(`Sign`) USING BTREE COMMENT '标记唯一索引'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of device_type
-- ----------------------------
INSERT INTO `device_type` VALUES (1, '物理服务器', 'S');
INSERT INTO `device_type` VALUES (2, '存储阵列', 'A');
INSERT INTO `device_type` VALUES (3, '一体化设备', 'D');
INSERT INTO `device_type` VALUES (4, '交换机', 'W');
INSERT INTO `device_type` VALUES (11, '虚拟服务器', 'V');

SET FOREIGN_KEY_CHECKS = 1;
