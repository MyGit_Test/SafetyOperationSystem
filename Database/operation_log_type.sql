/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/04/2021 11:49:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for operation_log_type
-- ----------------------------
DROP TABLE IF EXISTS `operation_log_type`;
CREATE TABLE `operation_log_type`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `LogTypeName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类名称',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of operation_log_type
-- ----------------------------
INSERT INTO `operation_log_type` VALUES (1, '系统部署');
INSERT INTO `operation_log_type` VALUES (2, '系统维护');
INSERT INTO `operation_log_type` VALUES (3, '应用部署');
INSERT INTO `operation_log_type` VALUES (4, '应用维护');
INSERT INTO `operation_log_type` VALUES (5, '硬件');
INSERT INTO `operation_log_type` VALUES (6, '备份');
INSERT INTO `operation_log_type` VALUES (7, '网络');
INSERT INTO `operation_log_type` VALUES (8, '巡检');

SET FOREIGN_KEY_CHECKS = 1;
