/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/01/2022 15:48:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for application
-- ----------------------------
DROP TABLE IF EXISTS `application`;
CREATE TABLE `application`  (
  `Id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Pid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0',
  `DeviceId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所在设备ID',
  `ApplicationTypeId` int(0) NOT NULL COMMENT '应用分类ID',
  `AppName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '应用名称',
  `Version` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '应用版本',
  `InstallTime` datetime(0) NULL DEFAULT NULL COMMENT '上架时间',
  `Company` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '厂商名称',
  `Contact` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系方式',
  `DepartmentId` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '使用部门ID',
  `LinkMan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人',
  `Phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
  `Email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `Port` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '使用端口',
  `Record` bit(1) NOT NULL DEFAULT b'0' COMMENT '需要备案',
  `Level` int(0) NULL DEFAULT 0 COMMENT '重要定级',
  `Status` int(0) NOT NULL DEFAULT 1 COMMENT '状态',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `Creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fk_application_applicationtype`(`ApplicationTypeId`) USING BTREE,
  INDEX `fk_application_department`(`DepartmentId`) USING BTREE,
  CONSTRAINT `fk_application_applicationtype` FOREIGN KEY (`ApplicationTypeId`) REFERENCES `application_type` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_application_department` FOREIGN KEY (`DepartmentId`) REFERENCES `department` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for application_audit
-- ----------------------------
DROP TABLE IF EXISTS `application_audit`;
CREATE TABLE `application_audit`  (
  `Id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Annual` int(0) NOT NULL COMMENT '年度',
  `ApplicationId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '应用id',
  `AppName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '应用名称',
  `InstallTime` datetime(0) NULL DEFAULT NULL COMMENT '上架日期',
  `Status` int(0) NOT NULL DEFAULT 0 COMMENT '备案状态',
  `DepartmentId` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '部门id',
  `DepartmentName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '使用部门',
  `Url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '链接地址',
  `Remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `Auditor` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备案人',
  `AuditTime` datetime(0) NULL DEFAULT NULL COMMENT '备案时间',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for application_type
-- ----------------------------
DROP TABLE IF EXISTS `application_type`;
CREATE TABLE `application_type`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ApplicationTypeName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类名称',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for application_url
-- ----------------------------
DROP TABLE IF EXISTS `application_url`;
CREATE TABLE `application_url`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `ApplicationId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '应用系统',
  `Description` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '描述',
  `Url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '链接地址',
  `Creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fk_applicationurl_application`(`ApplicationId`) USING BTREE,
  CONSTRAINT `fk_applicationurl_application` FOREIGN KEY (`ApplicationId`) REFERENCES `application` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`  (
  `Id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `Contents` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '内容',
  `ArticleClassId` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '类型id',
  `Publisher` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `Creator` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `AppId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '相关应用id',
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fk_article_articleclass`(`ArticleClassId`) USING BTREE,
  CONSTRAINT `fk_article_articleclass` FOREIGN KEY (`ArticleClassId`) REFERENCES `article_class` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for article_class
-- ----------------------------
DROP TABLE IF EXISTS `article_class`;
CREATE TABLE `article_class`  (
  `Id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClassName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '文章类别',
  `Category` int(0) NULL DEFAULT NULL COMMENT '范畴',
  `OrderNo` int(0) NULL DEFAULT 0 COMMENT '排序',
  `Shown` bit(1) NULL DEFAULT b'1' COMMENT '显示',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for default_parameter
-- ----------------------------
DROP TABLE IF EXISTS `default_parameter`;
CREATE TABLE `default_parameter`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `DeviceTypeId` int(0) NOT NULL COMMENT '设备类型',
  `ParamName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '参数名称',
  `UnitName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '单位名称',
  `OrderNo` int(0) NULL DEFAULT 0 COMMENT '顺序号',
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fk_defaultparameter_devicetype`(`DeviceTypeId`) USING BTREE,
  CONSTRAINT `fk_defaultparameter_devicetype` FOREIGN KEY (`DeviceTypeId`) REFERENCES `device_type` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 500 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `Id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `DepartmentName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门名称',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for device
-- ----------------------------
DROP TABLE IF EXISTS `device`;
CREATE TABLE `device`  (
  `Id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `DeviceBatchId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '设备批次ID',
  `ServerNo` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '编码',
  `ServerName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '设备名称',
  `SeriesNumber` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '序列号',
  `IsTrust` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否托管',
  `DepartmentId` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '责任单位ID',
  `LinkMan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人',
  `Phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
  `Level` int(0) NULL DEFAULT 0 COMMENT '重要定级',
  `Status` int(0) NULL DEFAULT 1 COMMENT '状态',
  `RoomId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '机房',
  `Position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '位置',
  `Creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `IsVm` bit(1) NOT NULL DEFAULT b'0' COMMENT '虚拟化',
  `OsId` int(0) NULL DEFAULT NULL COMMENT '操作系统',
  `OsVersion` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作系统版本',
  `KVM` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'kvm编号',
  `AvailableStorage` decimal(10, 1) NULL DEFAULT 0.0 COMMENT '有效存储',
  `SwitchType` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '交换机用途',
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `uk_device_serverno`(`ServerNo`) USING BTREE,
  INDEX `fk_device_devicebatch`(`DeviceBatchId`) USING BTREE,
  INDEX `fk_device_department`(`DepartmentId`) USING BTREE,
  INDEX `fk_device_os`(`OsId`) USING BTREE,
  INDEX `fk_device_room`(`RoomId`) USING BTREE,
  CONSTRAINT `fk_device_department` FOREIGN KEY (`DepartmentId`) REFERENCES `department` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_device_devicebatch` FOREIGN KEY (`DeviceBatchId`) REFERENCES `device_batch` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_device_os` FOREIGN KEY (`OsId`) REFERENCES `os` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_device_room` FOREIGN KEY (`RoomId`) REFERENCES `room` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for device_batch
-- ----------------------------
DROP TABLE IF EXISTS `device_batch`;
CREATE TABLE `device_batch`  (
  `Id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `BatchNo` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '批次编码',
  `DeviceTypeId` int(0) NOT NULL COMMENT '设备类型',
  `ManufacturerId` int(0) NOT NULL COMMENT '厂商ID',
  `Model` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '型号',
  `Amount` int(0) NOT NULL COMMENT '数量',
  `InTime` datetime(0) NULL DEFAULT NULL COMMENT '入库日期',
  `InMan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '入库接收人',
  `ComeFrom` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '来源',
  `Creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `uk_devicebatch_batchno`(`BatchNo`) USING BTREE,
  INDEX `fk_devicebatch_devicetype`(`DeviceTypeId`) USING BTREE,
  INDEX `fk_devicebatch_manufacturer`(`ManufacturerId`) USING BTREE,
  CONSTRAINT `fk_devicebatch_devicetype` FOREIGN KEY (`DeviceTypeId`) REFERENCES `device_type` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_devicebatch_manufacturer` FOREIGN KEY (`ManufacturerId`) REFERENCES `manufacturer` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for device_ip
-- ----------------------------
DROP TABLE IF EXISTS `device_ip`;
CREATE TABLE `device_ip`  (
  `IpAddressId` int(0) NOT NULL COMMENT 'IP地址ID',
  `DeviceId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '设备ID',
  `DeviceTypeId` int(0) NOT NULL COMMENT '设备类型',
  `Creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`IpAddressId`) USING BTREE,
  INDEX `fk_deviceip_deviceid`(`DeviceId`) USING BTREE,
  INDEX `fk_deviceip_devicetype`(`DeviceTypeId`) USING BTREE,
  CONSTRAINT `fk_deviceip_devicetype` FOREIGN KEY (`DeviceTypeId`) REFERENCES `device_type` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_deviceip_ipaddress` FOREIGN KEY (`IpAddressId`) REFERENCES `ip_address` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for device_log
-- ----------------------------
DROP TABLE IF EXISTS `device_log`;
CREATE TABLE `device_log`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `LogType` int(0) NOT NULL COMMENT '日志类型',
  `LogTypeDesc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '日志类型描述',
  `DeviceId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '设备/应用ID',
  `DeviceName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备/应用名称',
  `OutMan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '出库交接人',
  `OutTime` date NULL DEFAULT NULL COMMENT '出库日期',
  `OutPhone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '交接人电话',
  `OutCreator` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '出库办理人',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `Creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for device_parameter
-- ----------------------------
DROP TABLE IF EXISTS `device_parameter`;
CREATE TABLE `device_parameter`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `DeviceTypeId` int(0) NOT NULL COMMENT '设备类型ID',
  `DeviceId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '设备ID',
  `ParamName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '参数名称',
  `ParamValue` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '参数值',
  `UnitName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '单位名称',
  `OrderNo` int(0) NULL DEFAULT 0 COMMENT '顺序号',
  `Creator` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fk_deviceparameter_devicetype`(`DeviceTypeId`) USING BTREE,
  CONSTRAINT `fk_deviceparameter_devicetype` FOREIGN KEY (`DeviceTypeId`) REFERENCES `device_type` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for device_type
-- ----------------------------
DROP TABLE IF EXISTS `device_type`;
CREATE TABLE `device_type`  (
  `Id` int(0) NOT NULL COMMENT '主键',
  `DeviceTypeName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '类型名称',
  `Sign` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标记字母',
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `uk_devicetype_sign`(`Sign`) USING BTREE COMMENT '标记唯一索引'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dns
-- ----------------------------
DROP TABLE IF EXISTS `dns`;
CREATE TABLE `dns`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `DomainName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '域名',
  `DomainDesc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `StartDate` date NULL DEFAULT NULL COMMENT '启用日期',
  `Disabled` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否下线',
  `EndDate` date NULL DEFAULT NULL COMMENT '下线日期',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ip_address
-- ----------------------------
DROP TABLE IF EXISTS `ip_address`;
CREATE TABLE `ip_address`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `Ip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'IP地址',
  `PoolId` int(0) NOT NULL COMMENT '地址池ID',
  `OpenType` int(0) NOT NULL DEFAULT 0 COMMENT '访问方式',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `Creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fk_ipaddress_ippool`(`PoolId`) USING BTREE,
  CONSTRAINT `fk_ipaddress_ippool` FOREIGN KEY (`PoolId`) REFERENCES `ip_pool` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ip_dns
-- ----------------------------
DROP TABLE IF EXISTS `ip_dns`;
CREATE TABLE `ip_dns`  (
  `DnsId` int(0) NOT NULL COMMENT 'dns表主键',
  `IpAddressId` int(0) NOT NULL COMMENT 'ip_address表主键',
  PRIMARY KEY (`DnsId`, `IpAddressId`) USING BTREE,
  INDEX `fk_ipdns_ipaddress`(`IpAddressId`) USING BTREE,
  CONSTRAINT `fk_ipdns_dns` FOREIGN KEY (`DnsId`) REFERENCES `dns` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_ipdns_ipaddress` FOREIGN KEY (`IpAddressId`) REFERENCES `ip_address` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ip_domain
-- ----------------------------
DROP TABLE IF EXISTS `ip_domain`;
CREATE TABLE `ip_domain`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `DomainName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '范畴名称',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `Creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ip_isp
-- ----------------------------
DROP TABLE IF EXISTS `ip_isp`;
CREATE TABLE `ip_isp`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `IspNo` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '线路编号',
  `IspName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '线路名称',
  `Contacts` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人',
  `Phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电话',
  `StartTime` date NULL DEFAULT NULL COMMENT '开通日期',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ip_pool
-- ----------------------------
DROP TABLE IF EXISTS `ip_pool`;
CREATE TABLE `ip_pool`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `DomainId` int(0) NOT NULL COMMENT '范畴ID',
  `IpSegment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '网络',
  `Mask` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子网',
  `Gateway` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '网关',
  `Place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '使用地点',
  `Remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `IpNumber` int(0) NOT NULL COMMENT 'ip个数',
  `Creator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fk_ippool_ipdomain`(`DomainId`) USING BTREE,
  CONSTRAINT `fk_ippool_ipdomain` FOREIGN KEY (`DomainId`) REFERENCES `ip_domain` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ip_port
-- ----------------------------
DROP TABLE IF EXISTS `ip_port`;
CREATE TABLE `ip_port`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `IpAddressId` int(0) NOT NULL COMMENT 'ip地址ID',
  `Protocal` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '协议名称',
  `Port` int(0) NOT NULL COMMENT '开放端口',
  `OpenType` int(0) NOT NULL DEFAULT 0 COMMENT '开放方式',
  `OpenRange` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '开放IP范围',
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fk_deviceport_ipaddress`(`IpAddressId`) USING BTREE,
  CONSTRAINT `fk_deviceport_ipaddress` FOREIGN KEY (`IpAddressId`) REFERENCES `ip_address` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for log_sys
-- ----------------------------
DROP TABLE IF EXISTS `log_sys`;
CREATE TABLE `log_sys`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `VisitTime` datetime(0) NOT NULL COMMENT '访问时间',
  `UserName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作用户',
  `Ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '访问ip',
  `Url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '访问资源url',
  `requestMethod` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'request方法',
  `ExecutionTime` int(0) NULL DEFAULT NULL COMMENT '执行时长',
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '访问方法',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9886 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for login_role
-- ----------------------------
DROP TABLE IF EXISTS `login_role`;
CREATE TABLE `login_role`  (
  `RoleId` int(0) NOT NULL COMMENT '主键',
  `RoleName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`RoleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for login_user
-- ----------------------------
DROP TABLE IF EXISTS `login_user`;
CREATE TABLE `login_user`  (
  `UserId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `Account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户账号',
  `Pwd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `RealName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '姓名',
  `Phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `LoginTimes` int(0) NULL DEFAULT 0 COMMENT '登陆次数',
  `LastLoginIP` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后登陆IP',
  `LastLoginTime` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `Disabled` bit(1) NULL DEFAULT b'1' COMMENT '禁用',
  `DepartmentId` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门id',
  PRIMARY KEY (`UserId`) USING BTREE,
  UNIQUE INDEX `uk_loginuser_account`(`Account`) USING BTREE COMMENT '用户名唯一索引',
  INDEX `fk_loginuser_department`(`DepartmentId`) USING BTREE COMMENT '部门外键索引',
  CONSTRAINT `fk_loginuser_department` FOREIGN KEY (`DepartmentId`) REFERENCES `department` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for manufacturer
-- ----------------------------
DROP TABLE IF EXISTS `manufacturer`;
CREATE TABLE `manufacturer`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ManufacturerName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '厂商名称',
  `BriefName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '简称',
  `Phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '客服电话',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for operation_log
-- ----------------------------
DROP TABLE IF EXISTS `operation_log`;
CREATE TABLE `operation_log`  (
  `Id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Operator` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作人',
  `DeviceTypeId` int(0) NULL DEFAULT NULL COMMENT '设备类型id',
  `DeviceName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '设备名称',
  `DeviceId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备id',
  `Ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备IP',
  `OperationLogTypeId` int(0) NOT NULL COMMENT '日志类别',
  `Contents` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作记录',
  `OperationDate` date NOT NULL COMMENT '操作日期',
  `Creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for operation_log_type
-- ----------------------------
DROP TABLE IF EXISTS `operation_log_type`;
CREATE TABLE `operation_log_type`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `LogTypeName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类名称',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for os
-- ----------------------------
DROP TABLE IF EXISTS `os`;
CREATE TABLE `os`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `OSName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作系统名称',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 99 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu`  (
  `menuId` int(0) NOT NULL COMMENT '菜单id',
  `roleId` int(0) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`menuId`, `roleId`) USING BTREE,
  INDEX `fk_rolemenu_loginrole`(`roleId`) USING BTREE,
  CONSTRAINT `fk_rolemenu_loginrole` FOREIGN KEY (`roleId`) REFERENCES `login_role` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rolemenu_sysmenu` FOREIGN KEY (`menuId`) REFERENCES `sys_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user`  (
  `UserId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户Id',
  `RoleId` int(0) NOT NULL COMMENT '角色Id',
  PRIMARY KEY (`UserId`, `RoleId`) USING BTREE,
  INDEX `fk_userrole_loginrole`(`RoleId`) USING BTREE,
  CONSTRAINT `fk_userrole_loginrole` FOREIGN KEY (`RoleId`) REFERENCES `login_role` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_userrole_loginuser` FOREIGN KEY (`UserId`) REFERENCES `login_user` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for room
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room`  (
  `Id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `RoomName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '机房名称',
  `CO2Fire` int(0) NULL DEFAULT 0 COMMENT '二氧化碳灭火器数量',
  `PowderFire` int(0) NULL DEFAULT 0 COMMENT '干粉灭火器数量',
  `FoamFire` int(0) NULL DEFAULT 0 COMMENT '泡沫灭火器数量',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `Id` int(0) NOT NULL,
  `AppAuditYear` int(0) NULL DEFAULT NULL COMMENT '备案年度',
  `AppAuditStart` datetime(0) NULL DEFAULT NULL COMMENT '备案开始时间',
  `AppAuditEnd` datetime(0) NULL DEFAULT NULL COMMENT '备案结束时间',
  `AppAuditOpen` bit(1) NULL DEFAULT b'0' COMMENT '备案开关',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parentId` int(0) NOT NULL DEFAULT 0 COMMENT '父菜单id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名称',
  `css` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标样式',
  `href` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '链接地址',
  `type` int(0) NOT NULL COMMENT '菜单类型',
  `permission` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '按钮权限',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `hidden` bit(1) NULL DEFAULT b'0' COMMENT '隐藏',
  `disabled` bit(1) NULL DEFAULT b'0' COMMENT '禁用',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 183 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for vm_server
-- ----------------------------
DROP TABLE IF EXISTS `vm_server`;
CREATE TABLE `vm_server`  (
  `Id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DeviceId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所在服务器',
  `ServerNo` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '编码',
  `ServerName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '服务器名称',
  `OsId` int(0) NULL DEFAULT NULL COMMENT '操作系统',
  `OsVersion` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作系统版本',
  `DepartmentId` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '责任单位',
  `LinkMan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人',
  `Phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电话',
  `Level` int(0) NULL DEFAULT 0 COMMENT '重要定级',
  `Status` int(0) NULL DEFAULT 1 COMMENT '状态',
  `InTime` datetime(0) NULL DEFAULT NULL COMMENT '入库时间',
  `Creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `CreateTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `uk_vmserver_serverno`(`ServerNo`) USING BTREE,
  INDEX `fk_vmserver_device`(`DeviceId`) USING BTREE,
  INDEX `fk_vmserver_department`(`DepartmentId`) USING BTREE,
  INDEX `fk_vmserver_os`(`OsId`) USING BTREE,
  CONSTRAINT `fk_vmserver_department` FOREIGN KEY (`DepartmentId`) REFERENCES `department` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_vmserver_device` FOREIGN KEY (`DeviceId`) REFERENCES `device` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_vmserver_os` FOREIGN KEY (`OsId`) REFERENCES `os` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- View structure for v_device
-- ----------------------------
DROP VIEW IF EXISTS `v_device`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_device` AS select `device`.`Id` AS `Id`,`device`.`ServerNo` AS `ServerNo`,`device`.`ServerName` AS `ServerName`,`device`.`Creator` AS `Creator`,`device`.`CreateTime` AS `CreateTime` from `device` where (`device`.`Status` = 1) union select `vm_server`.`Id` AS `Id`,`vm_server`.`ServerNo` AS `ServerNo`,`vm_server`.`ServerName` AS `ServerName`,`vm_server`.`Creator` AS `Creator`,`vm_server`.`CreateTime` AS `CreateTime` from `vm_server` where (`vm_server`.`Status` = 1);

-- ----------------------------
-- Procedure structure for applicationaudit_born
-- ----------------------------
DROP PROCEDURE IF EXISTS `applicationaudit_born`;
delimiter ;;
CREATE PROCEDURE `applicationaudit_born`(OUT p_rows INT,IN p_annual INT)
BEGIN
  -- 生成需要年度审核的应用系统
	-- p_rows：生成记录数量
	-- p_annual：年度
	
  DECLARE _appId VARCHAR (32) DEFAULT NULL;
  DECLARE _appName VARCHAR (50) DEFAULT NULL ;
  DECLARE _installTime DATETIME DEFAULT NULL ;
  DECLARE _departmentId VARCHAR (10) DEFAULT NULL ;
  DECLARE _departmentName VARCHAR (50) DEFAULT NULL ;
  DECLARE _description VARCHAR (50) DEFAULT NULL ;
  DECLARE _url VARCHAR (200) DEFAULT NULL ;
	DECLARE _auditUrl VARCHAR(500) DEFAULT NULL;
	
  DECLARE done1 INT DEFAULT TRUE ;
	
  DECLARE cur_audit CURSOR FOR 
	select application.id,appName,installTime,departmentId,departmentName FROM application 	
	LEFT JOIN department on application.departmentId=department.id 
	WHERE status=1 and record=1 AND application.id NOT IN (SELECT applicationId FROM application_audit WHERE annual=p_annual) ;
  
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1=FALSE;
	set p_rows=0;
	-- 打开游标
	OPEN cur_audit;
	FETCH cur_audit INTO _appId,_appName,_installTime,_departmentId,_departmentName ;
	WHILE done1 DO
		set _auditUrl='';
		BEGIN
		DECLARE done2 INT DEFAULT TRUE ;	
		-- 声明链接游标
	  DECLARE cur_url CURSOR FOR SELECT description,url FROM application_url WHERE applicationId=_appid ;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2=FALSE ;
		OPEN cur_url;
		FETCH cur_url INTO _description,_url ;
		WHILE done2 DO
			set _auditUrl= CONCAT(IFNULL(_auditUrl,''),_description,'|',_url,',') ; 
			FETCH cur_url INTO _description,_url ;
		END WHILE ;
		CLOSE cur_url ;
		-- 删除首尾字符
		SET _auditUrl=TRIM(BOTH ',' FROM _auditUrl);
		-- 插入记录
		INSERT INTO application_audit (id,annual,applicationId,appName,installTime,departmentId,departmentName,url)
			VALUES (replace(uuid(),'-',''),p_annual,_appId,_appName,_installTime,_departmentId,_departmentName,_auditUrl);
			
		SET p_rows=p_rows+1 ;
		END;
		
		FETCH cur_audit INTO _appId,_appName,_installTime,_departmentId,_departmentName ;
	END WHILE ;

  -- 关闭游标
  CLOSE cur_audit ;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_vmserver_delete
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_vmserver_delete`;
delimiter ;;
CREATE PROCEDURE `p_vmserver_delete`(IN `v_id` varchar(32))
BEGIN
	DECLARE _result INT DEFAULT 1;
	/** 标记是否出错 */ 
	declare t_error int default 0; 
	/** 如果出现sql异常，则将t_error设置为1后继续执行后面的操作 */ 
	declare continue handler for sqlexception set t_error=1; -- 出错处理 
	/** 显示的开启事务，启动它后，autocommit值会自动设置为0 */ 
	start transaction; 
	delete from device_ip where deviceId=v_id;
	delete from device_parameter where deviceId=v_id;
	delete from vm_server where id=v_id;
	/** 标记被改变,表示事务应该回滚 */ 
	if t_error=1 then 
		set _result=0;
		rollback; -- 事务回滚 
	else 
		commit; -- 事务提交 
	end if; 
	
	select _result;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_vmserver_offline
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_vmserver_offline`;
delimiter ;;
CREATE PROCEDURE `p_vmserver_offline`(IN v_id VARCHAR(32))
BEGIN
	DECLARE _result INT DEFAULT 1;
	/** 标记是否出错 */ 
	declare t_error int default 0; 
	/** 如果出现sql异常，则将t_error设置为1后继续执行后面的操作 */ 
	declare continue handler for sqlexception set t_error=1; -- 出错处理 
	/** 显示的开启事务，启动它后，autocommit值会自动设置为0 */ 
	start transaction; 
	delete from device_ip where deviceId=v_id;
	update vm_server set `status`=0 where id=v_id;
	/** 标记被改变,表示事务应该回滚 */ 
	if t_error=1 then 
		set _result=0;
		rollback; -- 事务回滚 
	else 
		commit; -- 事务提交 
	end if; 
	
	select _result;
	
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table device
-- ----------------------------
DROP TRIGGER IF EXISTS `tr_delete_device`;
delimiter ;;
CREATE TRIGGER `tr_delete_device` BEFORE DELETE ON `device` FOR EACH ROW begin
	delete from device_ip where deviceId=old.Id;
	delete from device_parameter where DeviceId=old.Id;
end
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table vm_server
-- ----------------------------
DROP TRIGGER IF EXISTS `tr_delete_vmserver`;
delimiter ;;
CREATE TRIGGER `tr_delete_vmserver` BEFORE DELETE ON `vm_server` FOR EACH ROW begin
	delete from device_ip where deviceId=old.Id;
	delete from device_parameter where deviceId=old.Id;
end
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table vm_server
-- ----------------------------
DROP TRIGGER IF EXISTS `tr_update_vmserver`;
delimiter ;;
CREATE TRIGGER `tr_update_vmserver` AFTER UPDATE ON `vm_server` FOR EACH ROW BEGIN
	if old.`Status`=1 and new.`Status`=0 then
		delete from device_ip where deviceId=old.Id;
	end if;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
