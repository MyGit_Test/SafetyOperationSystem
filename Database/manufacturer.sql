/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/04/2021 11:49:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for manufacturer
-- ----------------------------
DROP TABLE IF EXISTS `manufacturer`;
CREATE TABLE `manufacturer`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ManufacturerName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '厂商名称',
  `BriefName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '简称',
  `Phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '客服电话',
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manufacturer
-- ----------------------------
INSERT INTO `manufacturer` VALUES (1, '惠普', 'HP', NULL, NULL);
INSERT INTO `manufacturer` VALUES (2, '戴尔', 'DELL', NULL, NULL);
INSERT INTO `manufacturer` VALUES (3, 'IBM', NULL, NULL, NULL);
INSERT INTO `manufacturer` VALUES (4, '曙光', NULL, NULL, NULL);
INSERT INTO `manufacturer` VALUES (5, '浪潮', NULL, NULL, NULL);
INSERT INTO `manufacturer` VALUES (6, '联想', NULL, NULL, NULL);
INSERT INTO `manufacturer` VALUES (7, '华为', NULL, NULL, NULL);
INSERT INTO `manufacturer` VALUES (99, '其它', NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
