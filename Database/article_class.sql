/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/04/2021 11:48:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for article_class
-- ----------------------------
DROP TABLE IF EXISTS `article_class`;
CREATE TABLE `article_class`  (
  `Id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClassName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '文章类别',
  `Category` int(0) NULL DEFAULT NULL COMMENT '范畴',
  `OrderNo` int(0) NULL DEFAULT 0 COMMENT '排序',
  `Shown` bit(1) NULL DEFAULT b'1' COMMENT '显示',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_class
-- ----------------------------
INSERT INTO `article_class` VALUES ('101', 'Window系统', 1, 1, b'1');
INSERT INTO `article_class` VALUES ('102', 'Linux系统', 1, 2, b'1');
INSERT INTO `article_class` VALUES ('103', '应用系统', 1, 3, b'1');
INSERT INTO `article_class` VALUES ('104', '硬件', 1, 4, b'1');
INSERT INTO `article_class` VALUES ('105', '软件', 1, 5, b'1');
INSERT INTO `article_class` VALUES ('106', '网络技术', 1, 6, b'1');
INSERT INTO `article_class` VALUES ('110', '其他', 1, 10, b'1');

SET FOREIGN_KEY_CHECKS = 1;
