/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/04/2021 11:49:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for os
-- ----------------------------
DROP TABLE IF EXISTS `os`;
CREATE TABLE `os`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `OSName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作系统名称',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of os
-- ----------------------------
INSERT INTO `os` VALUES (1, 'Window Server');
INSERT INTO `os` VALUES (2, 'Window');
INSERT INTO `os` VALUES (3, 'EXSi');
INSERT INTO `os` VALUES (4, 'CentOS');
INSERT INTO `os` VALUES (5, 'SUSE');
INSERT INTO `os` VALUES (6, 'Debian');
INSERT INTO `os` VALUES (7, 'Red Hat');
INSERT INTO `os` VALUES (8, 'Linux');
INSERT INTO `os` VALUES (99, '其它');

SET FOREIGN_KEY_CHECKS = 1;
