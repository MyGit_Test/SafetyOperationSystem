/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/01/2022 16:02:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `Id` int(0) NOT NULL,
  `AppAuditYear` int(0) NULL DEFAULT NULL COMMENT '备案年度',
  `AppAuditStart` datetime(0) NULL DEFAULT NULL COMMENT '备案开始时间',
  `AppAuditEnd` datetime(0) NULL DEFAULT NULL COMMENT '备案结束时间',
  `AppAuditOpen` bit(1) NULL DEFAULT b'0' COMMENT '备案开关',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 2021, '2021-03-21 11:55:00', '2021-03-23 13:50:00', b'1');

SET FOREIGN_KEY_CHECKS = 1;
