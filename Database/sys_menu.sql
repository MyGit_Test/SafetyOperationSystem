/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/01/2022 16:02:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parentId` int(0) NOT NULL DEFAULT 0 COMMENT '父菜单id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名称',
  `css` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标样式',
  `href` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '链接地址',
  `type` int(0) NOT NULL COMMENT '菜单类型',
  `permission` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '按钮权限',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `hidden` bit(1) NULL DEFAULT b'0' COMMENT '隐藏',
  `disabled` bit(1) NULL DEFAULT b'0' COMMENT '禁用',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 183 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', 'icon-file', NULL, 1, NULL, 1, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (2, 1, '角色列表', NULL, '/loginRole/index', 1, NULL, 10, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (3, 1, '用户列表', NULL, '/loginUser/index', 1, NULL, 15, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (4, 3, '查询', NULL, NULL, 2, 'loginuser:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (5, 3, '新增', NULL, NULL, 2, 'loginuser:add', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (6, 3, '修改', NULL, NULL, 2, 'loginuser:edit', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (7, 3, '删除', NULL, NULL, 2, 'loginuser:delete', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (8, 3, '密码', NULL, NULL, 2, 'loginuser:password', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (9, 3, '角色', NULL, NULL, 2, 'loginuser:role', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (10, 2, '查询', NULL, NULL, 2, 'loginrole:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (11, 1, '权限菜单列表', NULL, '/sysMenu/index', 1, NULL, 20, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (12, 0, 'IP域名管理', 'icon-link', NULL, 1, NULL, 2, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (13, 0, '设备出入库', 'icon-book', NULL, 1, NULL, 3, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (14, 12, 'IP范畴', NULL, '/ipDomain/index', 1, NULL, 10, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (15, 12, 'IP地址池', NULL, '/ipPool/index', 1, NULL, 20, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (16, 2, '新增', NULL, NULL, 2, 'loginrole:add', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (17, 2, '修改', NULL, NULL, 2, 'loginrole:edit', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (18, 2, '删除', NULL, NULL, 2, 'loginrole:delete', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (19, 2, '权限', NULL, NULL, 2, 'loginrole:grant', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (20, 11, '查询', NULL, NULL, 2, 'sysmenu:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (21, 11, '新增', NULL, NULL, 2, 'sysmenu:add', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (22, 11, '编辑', NULL, NULL, 2, 'sysmenu:edit', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (23, 11, '删除', NULL, NULL, 2, 'sysmenu:delete', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (24, 1, '部门列表', NULL, '/department/index', 1, NULL, 25, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (25, 1, '机房列表', NULL, '/room/index', 1, NULL, 30, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (26, 1, '设备厂商列表', NULL, '/manufacturer/index', 1, NULL, 35, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (27, 1, '操作系统列表', NULL, '/os/index', 1, NULL, 40, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (28, 1, '应用类型列表', NULL, '/applicationType/index', 1, NULL, 45, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (29, 1, '文档分类列表', NULL, '/articleClass/index', 1, NULL, 50, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (30, 24, '查询', NULL, NULL, 2, 'department:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (31, 24, '新增', NULL, NULL, 2, 'department:add', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (32, 24, '编辑', NULL, NULL, 2, 'department:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (33, 24, '删除', NULL, NULL, 2, 'department:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (34, 25, '查询', NULL, NULL, 2, 'room:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (35, 25, '新增', NULL, NULL, 2, 'room:add', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (36, 25, '编辑', NULL, NULL, 2, 'room:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (37, 25, '删除', NULL, NULL, 2, 'room:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (38, 26, '查询', NULL, NULL, 2, 'manufacturer:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (39, 26, '新增', NULL, NULL, 2, 'manufacturer:add', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (40, 26, '修改', NULL, NULL, 2, 'manufacturer:edit', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (41, 26, '删除', NULL, NULL, 2, 'manufacturer:delete', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (42, 27, '查询', NULL, NULL, 2, 'os:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (43, 27, '新增', NULL, NULL, 2, 'os:add', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (44, 27, '编辑', NULL, NULL, 2, 'os:edit', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (45, 27, '删除', NULL, NULL, 2, 'os:delete', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (46, 28, '查询', NULL, NULL, 2, 'applicationtype:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (47, 28, '新增', NULL, NULL, 2, 'applicationtype:add', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (48, 28, '编辑', NULL, NULL, 2, 'applicationtype:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (49, 28, '删除', NULL, NULL, 2, 'applicationtype:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (50, 29, '查询', NULL, NULL, 2, 'articleclass:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (51, 29, '新增', NULL, NULL, 2, 'articleclass:add', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (52, 29, '编辑', NULL, NULL, 2, 'articleclass:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (53, 29, '删除', NULL, NULL, 2, 'articleclass:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (54, 0, '设备管理', 'icon-tasks', NULL, 1, NULL, 4, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (55, 0, '应用系统管理', 'icon-laptop', NULL, 1, NULL, 5, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (56, 12, 'IP地址', NULL, '/ipAddress/index', 1, NULL, 30, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (57, 12, '已配置IP', NULL, '/deviceIp/index', 1, NULL, 40, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (58, 14, '查询', NULL, NULL, 2, 'ipdomain:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (59, 14, '新增', NULL, NULL, 2, 'ipdomain:add', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (60, 14, '编辑', NULL, NULL, 2, 'ipdomain:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (61, 14, '删除', NULL, NULL, 2, 'ipdomain:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (62, 15, '查询', NULL, NULL, 2, 'ippool:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (63, 15, '新增', NULL, NULL, 2, 'ippool:add', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (64, 15, '编辑', NULL, NULL, 2, 'ippool:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (65, 15, '删除', NULL, NULL, 2, 'ippool:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (66, 15, '生成', NULL, NULL, 2, 'ippool:born', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (67, 56, '查询', NULL, NULL, 2, 'ipaddress:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (68, 56, '删除', NULL, NULL, 2, 'ipaddress:delete', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (69, 56, '编辑', NULL, NULL, 2, 'ipaddress:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (70, 56, '端口', NULL, NULL, 2, 'ipaddress:port', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (71, 54, '物理服务器列表', NULL, '/device/serverIndex', 1, NULL, 10, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (72, 54, '虚拟服务器列表', NULL, '/vmServer/index', 1, NULL, 20, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (73, 54, '存储阵列列表', NULL, '/device/storageIndex', 1, NULL, 30, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (74, 54, '一体化设备列表', NULL, '/device/wholeIndex', 1, NULL, 40, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (75, 54, '交换机列表', NULL, '/device/switchIndex', 1, NULL, 50, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (76, 55, '应用系统列表', NULL, '/application/index', 1, NULL, 10, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (77, 55, '应用备案列表', NULL, '/applicationAudit/index', 1, NULL, 20, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (78, 55, '年度备案', NULL, '/applicationAudit/indexAudit', 1, NULL, 30, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (79, 13, '设备入库管理', NULL, '/deviceBatch/index', 1, NULL, 10, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (80, 13, '设备出库管理', NULL, '/deviceOut/index', 1, NULL, 20, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (81, 79, '查询', NULL, NULL, 2, 'devicebatch:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (82, 79, '新增', NULL, NULL, 2, 'devicebatch:add', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (83, 79, '编辑', NULL, NULL, 2, 'devicebatch:edit', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (84, 79, '删除', NULL, NULL, 2, 'devicebatch:delete', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (85, 79, '录入设备', NULL, NULL, 2, 'devicebatch:device', 150, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (86, 71, '查询', NULL, NULL, 2, 'deviceserver:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (87, 71, '编辑', NULL, NULL, 2, 'deviceserver:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (89, 71, '录入应用', NULL, NULL, 2, 'deviceserver:app', 105, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (90, 71, 'IP地址', NULL, NULL, 2, 'deviceserver:ip', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (91, 71, '出库', NULL, NULL, 2, 'deviceserver:out', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (92, 72, '录入应用', NULL, NULL, 2, 'vmserver:app', 105, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (93, 72, '新增', NULL, NULL, 2, 'vmserver:add', 115, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (94, 72, '编辑', NULL, NULL, 2, 'vmserver:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (95, 72, '删除', NULL, NULL, 2, 'vmserver:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (96, 72, '查询', NULL, NULL, 2, 'vmserver:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (97, 72, 'IP地址', NULL, NULL, 2, 'vmserver:ip', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (98, 73, '录入应用', NULL, NULL, 2, 'devicestorage:app', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (99, 73, '查询', NULL, NULL, 2, 'devicestorage:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (100, 73, '编辑', NULL, NULL, 2, 'devicestorage:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (101, 73, '出库', NULL, NULL, 2, 'devicestorage:out', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (102, 73, 'IP地址', NULL, NULL, 2, 'devicestorage:ip', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (103, 74, '录入应用', NULL, NULL, 2, 'devicewhole:app', 105, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (104, 74, '编辑', NULL, NULL, 2, 'devicewhole:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (105, 74, '出库', NULL, NULL, 2, 'devicewhole:out', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (106, 74, 'IP地址', NULL, NULL, 2, 'devicewhole:ip', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (107, 74, '查询', NULL, NULL, 2, 'devicewhole:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (108, 75, '录入应用', NULL, NULL, 2, 'deviceswitch:app', 105, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (109, 75, '查询', NULL, NULL, 2, 'deviceswitch:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (110, 75, '编辑', NULL, NULL, 2, 'deviceswitch:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (111, 75, '出库', NULL, NULL, 2, 'deviceswitch:out', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (112, 75, 'IP地址', NULL, NULL, 2, 'deviceswitch:ip', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (113, 76, '查询', NULL, NULL, 2, 'application:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (114, 76, '编辑', NULL, NULL, 2, 'application:edit', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (115, 76, '下架', NULL, NULL, 2, 'application:down', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (116, 76, 'URL', NULL, NULL, 2, 'application:url', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (117, 76, '更换设备', NULL, NULL, 2, 'application:device', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (118, 76, '二级应用', NULL, NULL, 2, 'application:two', 150, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (119, 77, '查询', NULL, NULL, 2, 'applicationaudit:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (120, 77, '删除', NULL, NULL, 2, 'applicationaudit:delete', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (121, 77, '生成', NULL, NULL, 2, 'applicationaudit:born', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (122, 77, '设置', NULL, NULL, 2, 'applicationaudit:set', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (123, 57, '查询', NULL, NULL, 2, 'deviceip:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (124, 57, '删除', NULL, NULL, 2, 'deviceip:delete', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (125, 79, '查看', NULL, NULL, 2, 'devicebatch:detail', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (126, 71, '查看', NULL, NULL, 2, 'deviceserver:detail', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (127, 73, '查看', NULL, NULL, 2, 'devicestorage:detail', 105, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (128, 74, '查看', NULL, NULL, 2, 'devicewhole:detail', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (129, 75, '查看', NULL, NULL, 2, 'deviceswitch:detail', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (130, 72, '查看', NULL, NULL, 2, 'vmserver:detail', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (131, 71, '设备参数', NULL, NULL, 2, 'deviceserver:param', 150, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (132, 72, '设备参数', NULL, NULL, 2, 'vmserver:param', 150, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (133, 73, '设备参数', NULL, NULL, 2, 'devicewhole:param', 150, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (134, 74, '设备参数', NULL, NULL, 2, 'devicewhole:param', 150, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (135, 75, '设备参数', NULL, NULL, 2, 'deviceswitch:param', 150, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (136, 76, '查看', NULL, NULL, 2, 'application:detail', 105, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (137, 77, '编辑', NULL, NULL, 2, 'applicationaudit:edit', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (138, 78, '查询', NULL, NULL, 2, 'applicationaudit:auditquery', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (139, 78, '备案', NULL, NULL, 2, 'applicationaudit:audit', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (140, 0, '文档日志', 'icon-bookmark', NULL, 1, NULL, 6, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (141, 140, '操作日志', NULL, '/operationLog/index', 1, NULL, 10, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (142, 140, '技术文档', NULL, '/article/index', 1, NULL, 20, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (143, 141, '查询', NULL, NULL, 2, 'operationlog:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (144, 141, '新增', NULL, NULL, 2, 'operationlog:add', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (145, 141, '编辑', NULL, NULL, 2, 'operation:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (146, 141, '删除', NULL, NULL, 2, 'operationlog:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (147, 142, '查询', NULL, NULL, 2, 'article:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (148, 142, '查看', NULL, NULL, 2, 'article:detail', 105, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (149, 142, '新增', NULL, NULL, 2, 'article:add', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (150, 142, '编辑', NULL, NULL, 2, 'article:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (151, 142, '删除', NULL, NULL, 2, 'article:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (152, 79, '导出', NULL, NULL, 2, 'devicebatch:excel', 105, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (153, 71, '导出', NULL, NULL, 2, 'deviceserver:excel', 102, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (154, 72, '导出', NULL, NULL, 2, 'vmserver:excel', 102, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (155, 73, '导出', NULL, NULL, 2, 'devicestorage:excel', 102, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (156, 74, '导出', NULL, NULL, 2, 'devicewhole:excel', 102, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (157, 75, '导出', NULL, NULL, 2, 'deviceswitch:excel', 102, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (158, 80, '查询', NULL, NULL, 2, 'deviceout:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (159, 80, '导出', NULL, NULL, 2, 'deviceout:excel', 105, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (160, 80, '编辑', NULL, NULL, 2, 'deviceout:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (161, 80, '删除', NULL, NULL, 2, 'deviceout:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (163, 72, '下架', NULL, NULL, 2, 'vmserver:offline', 125, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (164, 76, '导出', NULL, NULL, 2, 'application:excel', 102, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (165, 77, '导出', NULL, NULL, 2, 'applicationaudit:excel', 102, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (166, 12, '域名管理', 'icon-rss', '/dns/index', 1, NULL, 50, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (167, 166, '查询', NULL, NULL, 2, 'dns:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (168, 166, '新增', NULL, NULL, 2, 'dns:add', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (169, 166, '编辑', NULL, NULL, 2, 'dns:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (170, 166, '删除', NULL, NULL, 2, 'dns:delete', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (172, 56, '域名', NULL, NULL, 2, 'ipaddress:dns', 140, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (173, 12, '运营商线路', NULL, '/ipIsp/index', 1, NULL, 60, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (174, 173, '查询', NULL, NULL, 2, 'ipisp:query', 100, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (175, 173, '新增', NULL, NULL, 2, 'ipisp:add', 110, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (176, 173, '编辑', NULL, NULL, 2, 'ipisp:edit', 120, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (177, 173, '删除', NULL, NULL, 2, 'ipisp:delete', 130, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (178, 0, '数据统计', 'icon-windows', NULL, 1, NULL, 7, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (179, 178, '服务器统计', NULL, '/statistics/serverFigure', 1, NULL, 10, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (180, 178, '应用系统统计', NULL, '/statistics/applicationFigure', 1, NULL, 20, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (181, 178, '存储统计', NULL, '/statistics/storageFigure', 1, NULL, 30, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (182, 178, '交换机统计', NULL, '/statistics/switchFigure', 1, NULL, 40, b'0', b'0', NULL);
INSERT INTO `sys_menu` VALUES (183, 178, 'IP地址统计', NULL, '/statistics/ipFigure', 1, NULL, 50, b'0', b'0', NULL);

SET FOREIGN_KEY_CHECKS = 1;
