﻿/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/01/2022 16:00:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for login_user
-- ----------------------------
DROP TABLE IF EXISTS `login_user`;
CREATE TABLE `login_user`  (
  `UserId` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `Account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户账号',
  `Pwd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `RealName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '姓名',
  `Phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `LoginTimes` int(0) NULL DEFAULT 0 COMMENT '登陆次数',
  `LastLoginIP` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后登陆IP',
  `LastLoginTime` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `Disabled` bit(1) NULL DEFAULT b'1' COMMENT '禁用',
  `DepartmentId` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门id',
  PRIMARY KEY (`UserId`) USING BTREE,
  UNIQUE INDEX `uk_loginuser_account`(`Account`) USING BTREE COMMENT '用户名唯一索引',
  INDEX `fk_loginuser_department`(`DepartmentId`) USING BTREE COMMENT '部门外键索引',
  CONSTRAINT `fk_loginuser_department` FOREIGN KEY (`DepartmentId`) REFERENCES `department` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login_user
-- ----------------------------
INSERT INTO `login_user` VALUES ('1', 'system', 'system', '超级管理员', NULL, 229, '127.0.0.1', '2021-10-26 16:23:05', b'0', '10');

SET FOREIGN_KEY_CHECKS = 1;
