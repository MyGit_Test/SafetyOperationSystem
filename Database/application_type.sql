/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/04/2021 11:48:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for application_type
-- ----------------------------
DROP TABLE IF EXISTS `application_type`;
CREATE TABLE `application_type`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ApplicationTypeName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类名称',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of application_type
-- ----------------------------
INSERT INTO `application_type` VALUES (1, '宣传展示');
INSERT INTO `application_type` VALUES (2, '管理办公');
INSERT INTO `application_type` VALUES (3, '教学科研');
INSERT INTO `application_type` VALUES (4, '生活服务');
INSERT INTO `application_type` VALUES (5, '一卡通');
INSERT INTO `application_type` VALUES (6, '安全维护');
INSERT INTO `application_type` VALUES (99, '其它');

SET FOREIGN_KEY_CHECKS = 1;
