/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : operations_management

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 12/04/2021 11:48:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for default_parameter
-- ----------------------------
DROP TABLE IF EXISTS `default_parameter`;
CREATE TABLE `default_parameter`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `DeviceTypeId` int(0) NOT NULL COMMENT '设备类型',
  `ParamName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '参数名称',
  `UnitName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '单位名称',
  `OrderNo` int(0) NULL DEFAULT 0 COMMENT '顺序号',
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fk_defaultparameter_devicetype`(`DeviceTypeId`) USING BTREE,
  CONSTRAINT `fk_defaultparameter_devicetype` FOREIGN KEY (`DeviceTypeId`) REFERENCES `device_type` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 500 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of default_parameter
-- ----------------------------
INSERT INTO `default_parameter` VALUES (1, 1, '产品类型', NULL, 1);
INSERT INTO `default_parameter` VALUES (2, 1, '产品结构', NULL, 2);
INSERT INTO `default_parameter` VALUES (3, 1, 'CPU类型', NULL, 3);
INSERT INTO `default_parameter` VALUES (4, 1, 'CPU频率', 'GHz', 4);
INSERT INTO `default_parameter` VALUES (5, 1, 'CPU数量', '颗', 5);
INSERT INTO `default_parameter` VALUES (6, 1, 'CPU核心', '个', 6);
INSERT INTO `default_parameter` VALUES (7, 1, '内存类型', NULL, 7);
INSERT INTO `default_parameter` VALUES (8, 1, '内存容量', 'GB', 8);
INSERT INTO `default_parameter` VALUES (9, 1, '硬盘数量', '个', 9);
INSERT INTO `default_parameter` VALUES (10, 1, '硬盘容量', 'GB', 10);
INSERT INTO `default_parameter` VALUES (11, 1, '硬盘转速', '转', 11);
INSERT INTO `default_parameter` VALUES (12, 1, '硬盘尺寸', '吋', 12);
INSERT INTO `default_parameter` VALUES (13, 1, 'RAID', NULL, 13);
INSERT INTO `default_parameter` VALUES (14, 1, '有效存储', 'GB', 14);
INSERT INTO `default_parameter` VALUES (15, 1, '网卡数量', '个', 15);
INSERT INTO `default_parameter` VALUES (16, 1, '电源数量', '个', 16);
INSERT INTO `default_parameter` VALUES (17, 1, '电源功率', 'W', 17);
INSERT INTO `default_parameter` VALUES (18, 1, '质保时间', '年', 18);
INSERT INTO `default_parameter` VALUES (19, 1, 'USB设备', NULL, 19);
INSERT INTO `default_parameter` VALUES (20, 2, '产品类型', NULL, 1);
INSERT INTO `default_parameter` VALUES (21, 2, '产品结构', NULL, 2);
INSERT INTO `default_parameter` VALUES (22, 2, 'CPU类型', NULL, 3);
INSERT INTO `default_parameter` VALUES (23, 2, 'CPU频率', 'GHz', 4);
INSERT INTO `default_parameter` VALUES (24, 2, 'CPU数量', '颗', 5);
INSERT INTO `default_parameter` VALUES (25, 2, 'CPU核心', '个', 6);
INSERT INTO `default_parameter` VALUES (26, 2, '内存类型', NULL, 7);
INSERT INTO `default_parameter` VALUES (27, 2, '内存容量', 'GB', 8);
INSERT INTO `default_parameter` VALUES (28, 2, '硬盘数量', '个', 9);
INSERT INTO `default_parameter` VALUES (29, 2, '硬盘容量', 'GB', 10);
INSERT INTO `default_parameter` VALUES (30, 2, '硬盘转速', '转', 11);
INSERT INTO `default_parameter` VALUES (31, 2, '硬盘尺寸', '吋', 12);
INSERT INTO `default_parameter` VALUES (32, 2, 'RAID', NULL, 13);
INSERT INTO `default_parameter` VALUES (33, 2, '有效存储', 'GB', 14);
INSERT INTO `default_parameter` VALUES (34, 2, '网卡数量', '个', 15);
INSERT INTO `default_parameter` VALUES (35, 2, '电源数量', '个', 16);
INSERT INTO `default_parameter` VALUES (36, 2, '电源功率', 'W', 17);
INSERT INTO `default_parameter` VALUES (37, 2, '质保时间', '年', 18);
INSERT INTO `default_parameter` VALUES (38, 2, 'USB设备', NULL, 19);
INSERT INTO `default_parameter` VALUES (51, 3, '产品类型', NULL, 1);
INSERT INTO `default_parameter` VALUES (52, 3, '产品结构', NULL, 2);
INSERT INTO `default_parameter` VALUES (53, 3, 'CPU类型', NULL, 3);
INSERT INTO `default_parameter` VALUES (54, 3, 'CPU频率', 'GHz', 4);
INSERT INTO `default_parameter` VALUES (55, 3, 'CPU数量', '颗', 5);
INSERT INTO `default_parameter` VALUES (56, 3, 'CPU核心', '个', 6);
INSERT INTO `default_parameter` VALUES (57, 3, '内存类型', NULL, 7);
INSERT INTO `default_parameter` VALUES (58, 3, '内存容量', 'GB', 8);
INSERT INTO `default_parameter` VALUES (59, 3, '硬盘数量', '个', 9);
INSERT INTO `default_parameter` VALUES (60, 3, '硬盘容量', 'GB', 10);
INSERT INTO `default_parameter` VALUES (61, 3, '硬盘转速', '转', 11);
INSERT INTO `default_parameter` VALUES (62, 3, '硬盘尺寸', '吋', 12);
INSERT INTO `default_parameter` VALUES (63, 3, 'RAID', NULL, 13);
INSERT INTO `default_parameter` VALUES (64, 3, '有效存储', 'GB', 14);
INSERT INTO `default_parameter` VALUES (65, 3, '网卡数量', '个', 15);
INSERT INTO `default_parameter` VALUES (66, 3, '电源数量', '个', 16);
INSERT INTO `default_parameter` VALUES (67, 3, '电源功率', 'W', 17);
INSERT INTO `default_parameter` VALUES (68, 3, '质保时间', '年', 18);
INSERT INTO `default_parameter` VALUES (69, 3, 'USB设备', NULL, 19);
INSERT INTO `default_parameter` VALUES (70, 3, '产品类型', NULL, 1);
INSERT INTO `default_parameter` VALUES (71, 3, '产品结构', NULL, 2);
INSERT INTO `default_parameter` VALUES (72, 3, 'CPU类型', NULL, 3);
INSERT INTO `default_parameter` VALUES (73, 3, 'CPU频率', 'GHz', 4);
INSERT INTO `default_parameter` VALUES (74, 3, 'CPU数量', '颗', 5);
INSERT INTO `default_parameter` VALUES (75, 3, 'CPU核心', '个', 6);
INSERT INTO `default_parameter` VALUES (76, 3, '内存类型', NULL, 7);
INSERT INTO `default_parameter` VALUES (77, 3, '内存容量', 'GB', 8);
INSERT INTO `default_parameter` VALUES (78, 3, '硬盘数量', '个', 9);
INSERT INTO `default_parameter` VALUES (79, 3, '硬盘容量', 'GB', 10);
INSERT INTO `default_parameter` VALUES (80, 3, '硬盘转速', '转', 11);
INSERT INTO `default_parameter` VALUES (81, 3, '硬盘尺寸', '吋', 12);
INSERT INTO `default_parameter` VALUES (82, 3, 'RAID', NULL, 13);
INSERT INTO `default_parameter` VALUES (83, 3, '有效存储', 'GB', 14);
INSERT INTO `default_parameter` VALUES (84, 3, '网卡数量', '个', 15);
INSERT INTO `default_parameter` VALUES (85, 3, '电源数量', '个', 16);
INSERT INTO `default_parameter` VALUES (86, 3, '电源功率', 'W', 17);
INSERT INTO `default_parameter` VALUES (87, 3, '质保时间', '年', 18);
INSERT INTO `default_parameter` VALUES (88, 3, 'USB设备', NULL, 19);
INSERT INTO `default_parameter` VALUES (114, 4, '产品类型', NULL, 1);
INSERT INTO `default_parameter` VALUES (115, 4, '产品结构', NULL, 2);
INSERT INTO `default_parameter` VALUES (116, 4, 'CPU类型', NULL, 3);
INSERT INTO `default_parameter` VALUES (117, 4, 'CPU频率', 'GHz', 4);
INSERT INTO `default_parameter` VALUES (118, 4, 'CPU数量', '颗', 5);
INSERT INTO `default_parameter` VALUES (119, 4, 'CPU核心', '个', 6);
INSERT INTO `default_parameter` VALUES (120, 4, '内存类型', NULL, 7);
INSERT INTO `default_parameter` VALUES (121, 4, '内存容量', 'GB', 8);
INSERT INTO `default_parameter` VALUES (122, 4, '硬盘数量', '个', 9);
INSERT INTO `default_parameter` VALUES (123, 4, '硬盘容量', 'GB', 10);
INSERT INTO `default_parameter` VALUES (124, 4, '硬盘转速', '转', 11);
INSERT INTO `default_parameter` VALUES (125, 4, '硬盘尺寸', '吋', 12);
INSERT INTO `default_parameter` VALUES (126, 4, 'RAID', NULL, 13);
INSERT INTO `default_parameter` VALUES (127, 4, '有效存储', 'GB', 14);
INSERT INTO `default_parameter` VALUES (128, 4, '网卡数量', '个', 15);
INSERT INTO `default_parameter` VALUES (129, 4, '电源数量', '个', 16);
INSERT INTO `default_parameter` VALUES (130, 4, '电源功率', 'W', 17);
INSERT INTO `default_parameter` VALUES (131, 4, '质保时间', '年', 18);
INSERT INTO `default_parameter` VALUES (132, 4, 'USB设备', NULL, 19);
INSERT INTO `default_parameter` VALUES (133, 4, '产品类型', NULL, 1);
INSERT INTO `default_parameter` VALUES (134, 4, '产品结构', NULL, 2);
INSERT INTO `default_parameter` VALUES (135, 4, 'CPU类型', NULL, 3);
INSERT INTO `default_parameter` VALUES (136, 4, 'CPU频率', 'GHz', 4);
INSERT INTO `default_parameter` VALUES (137, 4, 'CPU数量', '颗', 5);
INSERT INTO `default_parameter` VALUES (138, 4, 'CPU核心', '个', 6);
INSERT INTO `default_parameter` VALUES (139, 4, '内存类型', NULL, 7);
INSERT INTO `default_parameter` VALUES (140, 4, '内存容量', 'GB', 8);
INSERT INTO `default_parameter` VALUES (141, 4, '硬盘数量', '个', 9);
INSERT INTO `default_parameter` VALUES (142, 4, '硬盘容量', 'GB', 10);
INSERT INTO `default_parameter` VALUES (143, 4, '硬盘转速', '转', 11);
INSERT INTO `default_parameter` VALUES (144, 4, '硬盘尺寸', '吋', 12);
INSERT INTO `default_parameter` VALUES (145, 4, 'RAID', NULL, 13);
INSERT INTO `default_parameter` VALUES (146, 4, '有效存储', 'GB', 14);
INSERT INTO `default_parameter` VALUES (147, 4, '网卡数量', '个', 15);
INSERT INTO `default_parameter` VALUES (148, 4, '电源数量', '个', 16);
INSERT INTO `default_parameter` VALUES (149, 4, '电源功率', 'W', 17);
INSERT INTO `default_parameter` VALUES (150, 4, '质保时间', '年', 18);
INSERT INTO `default_parameter` VALUES (151, 4, 'USB设备', NULL, 19);
INSERT INTO `default_parameter` VALUES (152, 4, '产品类型', NULL, 1);
INSERT INTO `default_parameter` VALUES (153, 4, '产品结构', NULL, 2);
INSERT INTO `default_parameter` VALUES (154, 4, 'CPU类型', NULL, 3);
INSERT INTO `default_parameter` VALUES (155, 4, 'CPU频率', 'GHz', 4);
INSERT INTO `default_parameter` VALUES (156, 4, 'CPU数量', '颗', 5);
INSERT INTO `default_parameter` VALUES (157, 4, 'CPU核心', '个', 6);
INSERT INTO `default_parameter` VALUES (158, 4, '内存类型', NULL, 7);
INSERT INTO `default_parameter` VALUES (159, 4, '内存容量', 'GB', 8);
INSERT INTO `default_parameter` VALUES (160, 4, '硬盘数量', '个', 9);
INSERT INTO `default_parameter` VALUES (161, 4, '硬盘容量', 'GB', 10);
INSERT INTO `default_parameter` VALUES (162, 4, '硬盘转速', '转', 11);
INSERT INTO `default_parameter` VALUES (163, 4, '硬盘尺寸', '吋', 12);
INSERT INTO `default_parameter` VALUES (164, 4, 'RAID', NULL, 13);
INSERT INTO `default_parameter` VALUES (165, 4, '有效存储', 'GB', 14);
INSERT INTO `default_parameter` VALUES (166, 4, '网卡数量', '个', 15);
INSERT INTO `default_parameter` VALUES (167, 4, '电源数量', '个', 16);
INSERT INTO `default_parameter` VALUES (168, 4, '电源功率', 'W', 17);
INSERT INTO `default_parameter` VALUES (169, 4, '质保时间', '年', 18);
INSERT INTO `default_parameter` VALUES (170, 4, 'USB设备', NULL, 19);
INSERT INTO `default_parameter` VALUES (171, 4, '产品类型', NULL, 1);
INSERT INTO `default_parameter` VALUES (172, 4, '产品结构', NULL, 2);
INSERT INTO `default_parameter` VALUES (173, 4, 'CPU类型', NULL, 3);
INSERT INTO `default_parameter` VALUES (174, 4, 'CPU频率', 'GHz', 4);
INSERT INTO `default_parameter` VALUES (175, 4, 'CPU数量', '颗', 5);
INSERT INTO `default_parameter` VALUES (176, 4, 'CPU核心', '个', 6);
INSERT INTO `default_parameter` VALUES (177, 4, '内存类型', NULL, 7);
INSERT INTO `default_parameter` VALUES (178, 4, '内存容量', 'GB', 8);
INSERT INTO `default_parameter` VALUES (179, 4, '硬盘数量', '个', 9);
INSERT INTO `default_parameter` VALUES (180, 4, '硬盘容量', 'GB', 10);
INSERT INTO `default_parameter` VALUES (181, 4, '硬盘转速', '转', 11);
INSERT INTO `default_parameter` VALUES (182, 4, '硬盘尺寸', '吋', 12);
INSERT INTO `default_parameter` VALUES (183, 4, 'RAID', NULL, 13);
INSERT INTO `default_parameter` VALUES (184, 4, '有效存储', 'GB', 14);
INSERT INTO `default_parameter` VALUES (185, 4, '网卡数量', '个', 15);
INSERT INTO `default_parameter` VALUES (186, 4, '电源数量', '个', 16);
INSERT INTO `default_parameter` VALUES (187, 4, '电源功率', 'W', 17);
INSERT INTO `default_parameter` VALUES (188, 4, '质保时间', '年', 18);
INSERT INTO `default_parameter` VALUES (189, 4, 'USB设备', NULL, 19);
INSERT INTO `default_parameter` VALUES (496, 11, 'CPU总核数', '个', 1);
INSERT INTO `default_parameter` VALUES (497, 11, '内存容量', 'GB', 2);
INSERT INTO `default_parameter` VALUES (498, 11, '硬盘容量', 'GB', 3);
INSERT INTO `default_parameter` VALUES (499, 11, '网卡数量', '个', 4);
INSERT INTO `default_parameter` VALUES (500, 11, 'USP设备', NULL, 5);

SET FOREIGN_KEY_CHECKS = 1;
