/**
 *
 * @param {获取地址栏传递的参数} name
 */
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数

    if (r != null) return decodeURI(r[2]);
    return null;
}

/**
 * 验证不小于0的正整数
 * @param {要验证的数值} val
 */
function ValidatInteger(val) {
    var reg = /^[0-9]*$/;
    return reg.test(val);
}
/**
 * 验证保留1位小数的浮点型数值
 * @param {要验证的数值} val
 */
function ValidateFloatOne(val) {
    var reg = /^[0 - 9] + (.[0 - 9]{1})?$/;
    return reg.test(val);
}

/**
 * 验证ipv4地址
 * @param ip
 * @returns {boolean}
 */
function ValidateIPv4(ip) {
    var reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
    return reg.test(ip);
}
/**
 * 格式“是”or“否”
 * @param value
 * @returns {string|string}
 */
function formatYesOrNo(value) {
    return value == 1 ? '是' : (value == '0' ? '否' : null);
}
/**
 * 返回字符串长度，中文计数为2
 * @param str
 * @returns {number}
 */
function strLength(str) {
    let len = 0;
    for (let i = 0, length = str.length; i < length; i++) {
        str.charCodeAt(i) > 255 ? len += 2 : len++;
    }
    return len;
}
/**
 * 截取字符串
 * @param {any} str
 * @param {number} len
 * @param {number} endcount
 */
function subStr(str, len, endcount) {
    var result = '';
    if (str) {
        result = str;
        if (str.length > len) {
            result = str.substr(0, len)
            if(!endcount){
                endcount=3;
            }
            for (var i = 0; i < endcount; i++) {
                result += '.';
            }
        }
    }
    return result;
}
/**
 * 将空值null、undefined设置成空字符串
 * @param {string} str
* */
function resetNull(str) {
    if (str || str == 0 || str == false) {
        return str;
    }
    else {
        return '';
    }
}
/**
 * 判断对象是否为空，不为空时向下遍历，返回值
 * @param {object} obj 要处理的对象
 * @param {string} nameStr 对象名，如：teacher.department
 * */
function jsonObjValue(obj,nameStr) {
    var objNames = nameStr.split('.');
    var object = obj;
    for (let index=0; index<objNames.length;index++){
        const element = objNames[index];
        object = object[element];
        if(!object){
            break;
        }
    }
    return resetNull(object);
}

//html编码
function htmlEncode(value) {
    return $('<div/>').text(value).html();
}
//html解码
function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

/**
 * 把url中的双斜杠替换为单斜杠
 * 如:http://localhost:8080//api//demo.替换后http://localhost:8080/api/demo
 * @param url
 * @returns {string}
 */
function formatUrl(url) {
    let index = 0;
    if (url.startsWith('http')) {
        index = 7
    }
    return url.substring(0, index) + url.substring(index).replace(/\/\/*/g, '/');
}
/** 产生一个随机的32位长度字符串 */
function uuid() {
    let text = "";
    let possible = "abcdef0123456789";
    for (let i = 0; i < 19; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text + new Date().getTime();
}

/**
 * 相当于Array.map，从数组项的对象抽单独抽取一列，
 * 如：[{"name":"a","value":1},{"name":"b","value":2}]，抽取后形成[1,2]
 * @param {any} ary 要抽取的数组
 * @param {any} name    要抽取列的名称
 * @returns {Array}
 */
function arrayMap(ary, name) {
    var list = [];
    for (var i = 0; i < ary.length; i++) {
        list.push(ary[i][name]);
    }
    return list;
}
/**
 * description sort排序函数
 * @param {Object} key 数组排序字段名称
 * @param {Object} isAsc 升序
 **/
function sortExp(key, isAsc) {
    return function (x, y) {
        if (isNaN(key)) {
            if (x[key] > y[key]) {
                return 1 * (isAsc ? 1 : -1);

            } else if (x[key] < y[key]) {
                return -1 * (isAsc ? 1 : -1);
            } else {
                return 0;
            }

        } else {
            return (x[key] - y[key]) * (isAsc ? 1 : -1)
        }
    }
}

/**
 * 日期对象转为日期字符串
 * @param date 需要格式化的日期对象
 * @param sFormat 输出格式,默认为yyyy-MM-dd                        年：y，月：M，日：d，时：h，分：m，秒：s
 * @example  dateFormat(new Date())                               "2017-02-28"
 * @example  dateFormat(new Date(),'yyyy-MM-dd')                  "2017-02-28"
 * @example  dateFormat(new Date(),'yyyy-MM-dd HH:mm:ss')         "2017-02-28 13:24:00"   ps:HH:24小时制
 * @example  dateFormat(new Date(),'yyyy-MM-dd hh:mm:ss')         "2017-02-28 01:24:00"   ps:hh:12小时制
 * @example  dateFormat(new Date(),'hh:mm')                       "09:24"
 * @example  dateFormat(new Date(),'yyyy-MM-ddTHH:mm:ss+08:00')   "2017-02-28T13:24:00+08:00"
 * @example  dateFormat(new Date('2017-02-28 13:24:00'),'yyyy-MM-ddTHH:mm:ss+08:00')   "2017-02-28T13:24:00+08:00"
 * @returns {string}
 */
function dateFormat(date, sFormat) {
    if (!sFormat) {
        sFormat = 'yyyy-MM-dd';
    }
    let time = {
        Year: 0,
        TYear: '0',
        Month: 0,
        TMonth: '0',
        Day: 0,
        TDay: '0',
        Hour: 0,
        THour: '0',
        hour: 0,
        Thour: '0',
        Minute: 0,
        TMinute: '0',
        Second: 0,
        TSecond: '0',
        Millisecond: 0
    };
    time.Year = date.getFullYear();
    time.TYear = String(time.Year).substr(2);
    time.Month = date.getMonth() + 1;
    time.TMonth = time.Month < 10 ? "0" + time.Month : String(time.Month);
    time.Day = date.getDate();
    time.TDay = time.Day < 10 ? "0" + time.Day : String(time.Day);
    time.Hour = date.getHours();
    time.THour = time.Hour < 10 ? "0" + time.Hour : String(time.Hour);
    time.hour = time.Hour < 13 ? time.Hour : time.Hour - 12;
    time.Thour = time.hour < 10 ? "0" + time.hour : String(time.hour);
    time.Minute = date.getMinutes();
    time.TMinute = time.Minute < 10 ? "0" + time.Minute : String(time.Minute);
    time.Second = date.getSeconds();
    time.TSecond = time.Second < 10 ? "0" + time.Second : String(time.Second);
    time.Millisecond = date.getMilliseconds();

    return sFormat.replace(/yyyy/ig, String(time.Year))
        .replace(/yyy/ig, String(time.Year))
        .replace(/yy/ig, time.TYear)
        .replace(/y/ig, time.TYear)
        .replace(/MM/g, time.TMonth)
        .replace(/M/g, String(time.Month))
        .replace(/dd/ig, time.TDay)
        .replace(/d/ig, String(time.Day))
        .replace(/HH/g, time.THour)
        .replace(/H/g, String(time.Hour))
        .replace(/hh/g, time.Thour)
        .replace(/h/g, String(time.hour))
        .replace(/mm/g, time.TMinute)
        .replace(/m/g, String(time.Minute))
        .replace(/ss/ig, time.TSecond)
        .replace(/s/ig, String(time.Second))
        .replace(/fff/ig, String(time.Millisecond))
}

/**
 * 获取分页大小
 * @returns {number}
 */
function getPagesize(){
    var rows = sessionStorage.getItem('pagesize');
    var size = parseInt(rows);
    return (size&&size>=10)?size:10;
}

/**
 * 保存分页大小
 * @param pagesize
 */
function setPagesize(pagesize) {
    if(typeof(pagesize) == 'number'){
        sessionStorage.setItem('pagesize',pagesize);
    }
    else {
        sessionStorage.setItem('pagesize',10);
    }
}

function sessionStorageGetItem(key) {
    let jsonString = sessionStorage.getItem(key);
    if (jsonString) {
        return JSON.parse(jsonString);
    }
    return null;
}

function sessionStorageSetItem(key, value) {
    sessionStorage.setItem(key, JSON.stringify(value));
}

function sessionStorageRemoveItem(key) {
    sessionStorage.removeItem(key);
}

function sessionStorageClear() {
    sessionStorage.clear();
}
