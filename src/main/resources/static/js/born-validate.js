﻿﻿$.ajaxSetup({ cache: false });    //close AJAX cache

$.validator.setDefaults({
    debug: false,
    errorClass: 'text-danger', //错误信息使用的样式类
    errorElement: 'span',     //错误信息放置元素
    ignore: ".ignore",       //忽略验证原始
    showErrors: function (map, list) {
        this.currentElements.removeAttr("title").parent().removeClass("has-warning");
        $.each(list, function (index, error) {
            $(error.element).attr("title", error.message).parent().addClass("has-warning");
        });
        //console.log("未通过验证：" + this.numberOfInvalids());
        this.defaultShowErrors();
    },
    errorPlacement: function (error, element) {
        if(element.parent().is(".checkbox-inline") || element.parent().is(".radio-inline"))
            error.appendTo(element.parent().parent().parent());
        else if (element.parent().is(".input-group") || element.parent().is(".input-control")) {
            error.appendTo(element.parent().parent());
        }
        else if (element.parent().parent().parent().is(".check_group"))
            error.appendTo(element.parent().parent().parent().parent());
        else
            error.appendTo(element.parent());
    }
});

function bornValidate($form) {
    var rule = {};
    $form.find("input[type!='hidden']").each(function () {
        var tmp = {};
        // if ($(this).data('val-required')) {
        //     tmp['required'] = true;
        // }
        // if ($(this).data('val-length-max')) {
        //     tmp['maxlength'] = $(this).data('val-length-max');
        // }
        // if ($(this).data('val-number')) {
        //     tmp['number'] = true;
        // }
        if ($(this).attr('type') == 'email') {
            tmp['email'] = true;
        }
        if ($(this).attr('type') == 'datetime') {
            tmp['date'] = true;
        }
        if (Object.keys(tmp).length > 0) {
            rule[$(this).attr('name')] = tmp;
        }
    });
    // $form.find("textarea").each(function () {
    //     var tmp = {};
    //     if ($(this).data('val-length-max')) {
    //         tmp['maxlength'] = $(this).data('val-length-max');
    //     }
    //     if (Object.keys(tmp).length > 0) {
    //         rule[$(this).attr('name')] = tmp;
    //     }
    // });
    var valid = {};
    valid['rule'] = rule;
    return valid;
}

