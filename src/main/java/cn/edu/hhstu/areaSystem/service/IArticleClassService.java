package cn.edu.hhstu.areaSystem.service;

import cn.edu.hhstu.pojo.ArticleClass;

import java.util.List;

/**
 * @author wly
 * @version 1.0
 * @date 2021/3/19 17:08
 * @desc 无
 */
public interface IArticleClassService {
    public List<ArticleClass> list(Integer category) throws Exception;
    public List<ArticleClass> list() throws Exception;
    public ArticleClass detail(String id) throws Exception;
    public int insert(ArticleClass entity) throws Exception;
    public int update(ArticleClass entity) throws Exception;
    public int delete(String id) throws Exception;
}
