package cn.edu.hhstu.areaSystem.service;

import cn.edu.hhstu.pojo.LogSys;

/**
 * @author wly
 * @version 1.0
 * @date 2021/1/7 15:12
 * @desc 无
 */
public interface ISysLogService {

    public int insert(LogSys entity) throws Exception;

}
