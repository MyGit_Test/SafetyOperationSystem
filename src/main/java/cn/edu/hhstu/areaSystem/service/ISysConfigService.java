package cn.edu.hhstu.areaSystem.service;

import cn.edu.hhstu.entity.AppAuditConfigEntity;

/**
 * @author wly
 * @version 1.0
 * @date 2021/3/16 11:13
 * @desc 无
 */
public interface ISysConfigService {
    public AppAuditConfigEntity getAppAuditConfig() throws Exception;
    public int setAppAuditConfig(AppAuditConfigEntity entity) throws Exception;
}
