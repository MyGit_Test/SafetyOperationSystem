package cn.edu.hhstu.areaSystem.service;

import cn.edu.hhstu.pojo.DefaultParameter;

import java.util.List;

/**
 * @author wly
 * @version 1.0
 * @date 2021/3/9 15:53
 * @desc 无
 */
public interface IDefaultParameterService {
    public List<DefaultParameter> unuseList(Integer deviceTypeId,String deviceId) throws Exception;
    public List<DefaultParameter> list() throws Exception;
}
