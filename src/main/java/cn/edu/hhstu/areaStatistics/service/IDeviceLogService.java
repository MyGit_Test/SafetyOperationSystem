package cn.edu.hhstu.areaStatistics.service;

import cn.edu.hhstu.pojo.DeviceLog;

import java.util.HashMap;
import java.util.List;

/**
 * @author wly
 * @version 1.0
 * @date 2021/5/21 8:20
 * @desc 无
 */
public interface IDeviceLogService {
    public List<DeviceLog> list(HashMap params) throws Exception;
    public List<DeviceLog> list(String deviceId) throws Exception;
    public DeviceLog detail(int id) throws Exception;
    public int insert(DeviceLog entity) throws Exception;
    public int update(DeviceLog entity) throws Exception;
    public int delete(int id) throws Exception;
}
