package cn.edu.hhstu.areaStatistics.service;

import cn.edu.hhstu.entity.FigureEntity;

import java.util.List;

/**
 * @author wly
 * @version 1.0
 * @date 2021/10/26 15:56
 * @desc 无
 */
public interface IApplicationFigureService {
    public List<FigureEntity<Integer>> byApplicationType() throws Exception;
    public List<FigureEntity<Integer>> byDepartment() throws Exception;
}

