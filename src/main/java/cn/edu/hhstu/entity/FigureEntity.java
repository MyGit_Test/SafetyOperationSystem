package cn.edu.hhstu.entity;

import lombok.Data;

/**
 * @author wly
 * @version 1.0
 * @date 2021/10/25 17:12
 * @desc 数据统计结果实体类
 */
@Data
public class FigureEntity<T>{
    /// <summary>
    /// 范畴名称
    /// </summary>
    private  String subgroup;
    /// <summary>
    /// 分组名称
    /// </summary>
    private String name;
    /// <summary>
    /// 小类名称
    /// </summary>
    private  String kind;
    /// <summary>
    /// 统计值
    /// </summary>
    private T value;
    private T value1;
    private T value2;
}
