package cn.edu.hhstu.entity;

/**
 * @author wly
 * @version 1.0
 * @date 2021/2/8 11:07
 * @desc 无
 */
public class SelectItemsEntity {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;
}
