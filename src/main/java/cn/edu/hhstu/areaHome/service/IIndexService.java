package cn.edu.hhstu.areaHome.service;

import cn.edu.hhstu.security.pojo.LoginUser;

/**
 * @author wly
 * @version 1.0
 * @date 2021/3/29 17:01
 * @desc 无
 */
public interface IIndexService {
    public int serverCount(int deviceTypeId) throws Exception;
    public int serverTrustCount(String departmentId) throws Exception;
    public int vmServerCount() throws Exception;
    public int applicationCount() throws Exception;
    public int applicationCount(String departmentId) throws Exception;
    public int operationLogCount() throws Exception;

    public LoginUser userDetail(String id) throws Exception;
    public int passwordSave(String userId,String pwd) throws Exception;
}
