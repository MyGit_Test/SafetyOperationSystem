package cn.edu.hhstu.areaDocument.mapper;

import cn.edu.hhstu.pojo.OperationLogType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wly
 * @version 1.0
 * @date 2021/3/11 17:18
 * @desc 无
 */
@Mapper
@Repository
public interface IOperationLogTypeMapper {

    @Select("select * from operation_log_type")
    public List<OperationLogType> list() throws Exception;
}
