package cn.edu.hhstu.areaDocument.service;

import cn.edu.hhstu.pojo.OperationLogType;

import java.util.List;

/**
 * @author wly
 * @version 1.0
 * @date 2021/3/11 17:19
 * @desc 无
 */
public interface IOperationLogTypeService {
    public List<OperationLogType> list() throws Exception;
}
