package cn.edu.hhstu.areaDocument.service;

import cn.edu.hhstu.pojo.Article;

import java.util.HashMap;
import java.util.List;

/**
 * @author wly
 * @version 1.0
 * @date 2021/3/22 16:16
 * @desc 无
 */
public interface IArticleService {
    public List<Article> listPage(HashMap params, Integer page, Integer rows) throws Exception;
    public Article detail(String id) throws Exception;
    public int insert(Article entity) throws Exception;
    public int update(Article entity) throws Exception;
    public int delete(String id) throws Exception;
    public void deleteByIds(String[] ids) throws Exception;
}
