# 网络安全运维管理系统

#### 介绍
网络安全运维管理。主要实现核心机房服务器、应用系统等的运维管理。

#### 软件架构

语言：java

框架：springboot

连接池：druid

数据库：mysql8

安全认证框架：spring security

接口描述：swagger2

日志：AOP


#### 安装教程

1.  将项目打包成jar包（默认，打war包需要另行配置）

    (1) 使用集成工具打包
    
    (2) 项目根目录下使用maven指令打包
    ```
    mvn clean package -Dmaven.test.skip=true
    ```
    生成的jar包在项目的target目录下。
    
2.  将生成的jar包拷贝到linux服务器
    ```
    scp target/xxx.jatr root@xxx.xxx.xxx:/opt/javaapps
    ```
    
3.  进入jar包所在目录/opt/javaapps，执行命令
    ```
    java -jar xxx.jar
    
    #修改启动服务的端口
    java -jar -Dserver.port=8081 xxx.jar
    
    #指定运行环境
    java -jar -Dspring.profiles.active=prod xxx.jar
    
    #以后台服务方式启动
    nohup java -jar xxx.jar > /dev/null 2>&1 &
    或
    nohup java -jar xxxx.jar &
    ```
    
4.  cenos7中配置service
    ```
    #进入/etc/systemd/system目录
    cd /etc/systemd/system
    #新建项目jar的service
    vim xxx.service
    #xxx.service的文件内容如下：
    [Unit]
    Description=xxx
    After=syslog.target network.target
     
    [Service]
    Type=simple
     
    ExecStart=/usr/bin/java -jar /opt/javaapps/xxx.jar
    ExecStop=/bin/kill -15 $MAINPID 
     
    User=root
    Group=199620
     
    [Install]
    WantedBy=nulti-user.target
    
    启停自定义的service
    systemctl start xxx #启动，xxx为我们自定义服务名
    systemctl stop xxx #停止
    
    设置开机启动
    systemctl enable xxx
    ```
    
#### 使用说明

1.  druid监控组件
    ```
    http://localhost:8081/druid/login.html
    ```
    
2.  swagger接口地址
    ```
    http://localhost:8081/swagger-ui.html
    或
    http://localhost:8081/doc.html
    ```
    
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
